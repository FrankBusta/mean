const router = require('express').Router();

router.get('/', (req,res,next) =>{
	res.render('index.html');
});

router.get('/user', (req,res,next) =>{
	res.render('user.html');
});

module.exports = router;