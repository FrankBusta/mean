const router = require('express').Router();
const mongojs = require('mongojs');

const db = mongojs('mean-app');

router.get('/products',(req,res, next) =>{
	db.products.find((err,products)=>{
		if(err) {
			console.log('error: ',err);
			return next(err);
		}
		res.status(200).json(products);
	});
});

router.get('/products/:id', (req,res,next)=>{
	db.products.findOne({_id: mongojs.ObjectId(req.params.id)}, (err,product)=>{
		if(err) return next(err);
		res.status(200).json(product);
	});
});

router.post('/products',(req,res,next)=>{
	 var product = req.body;

	if(!product.name || !product.description){
		res.status(400).json({
			error:'in product object'
		});
	}else{
		db.products.save(product, (err,product)=>{
			if(err)return next(err);
			res.status(200).json(product);
		});
	}
});

router.delete('/products/:id',(req,res,next)=>{
	db.products.remove({_id: mongojs.ObjectId(req.params.id)},(err,result)=>{
		if(err) return next(err);
		res.json({result:result});
    });
});

router.put('/products/:id',(req,res,next)=>{
	const product = req.body;
	product._id = mongojs.ObjectId(req.params.id);
	console.log(product);
	db.products.update({product},(err,product)=>{
		if(err){
		 res.status(400).json('error',err);
		 return next(err);}
		res.status(200).json({result:product});
	})
})

module.exports = router;